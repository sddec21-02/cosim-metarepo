#!/bin/bash

qemu/aarch64-softmmu/qemu-system-aarch64 -M arm-generic-fdt-7series -m 1G -kernel buildroot/output/images/uImage -dtb buildroot/output/images/zynq-zc702.dtb --initrd buildroot/output/images/rootfs.cpio.gz -serial /dev/null -serial mon:stdio -display none -net nic -net nic -net user -machine-path buildroot/handles -icount 0,sleep=off -rtc clock=vm -sync-quantum 1000000
