.PHONY: buildroot systemc all clean install

SYSC_VERSION=systemc-2.3.2

all: buildroot systemc

install: installQemu installSystemC

buildroot:
	$(MAKE) -C buildroot/ BR2_EXTERNAL=../iio_br/ cosim_defconfig
	cd buildroot/ && utils/brmake && cd ../

systemc:
	$(MAKE) -C systemctlm-cosim-demo 

installQemu:
	cd qemu && ./configure --target-list="arm-softmmu,aarch64-softmmu,microblazeel-softmmu" --enable-fdt --disable-kvm --disable-xen && cd ../
	$(MAKE) -C qemu -j4
	$(MAKE) -C qemu install


installSystemC:
	wget https://www.accellera.org/images/downloads/standards/systemc/systemc-2.3.2.tar.gz && tar xf ${SYSC_VERSION}.tar.gz
	cd systemc-2.3.2 && CXXFLAGS=-std=c++11 ./configure && cd ../
	$(MAKE) -C systemc-2.3.2 -j4
	$(MAKE) -C systemc-2.3.2 install
	rm -f ${SYSC_VERSION}.tar.gz
	

clean:
	$(MAKE) -C buildroot/ clean
	$(MAKE) -C systemctlm-cosim-demo clean
	rm -f buildroot/.config* 

