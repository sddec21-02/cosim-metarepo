# Senior Design Group 2
Iowa State University
Department of Electrical and Computer Engineering
Spring 2021

## Client
-  Mathew Weber -- Collins Aerospace

## Faculty Advisor
-  Dr. Philip Jones -- Electrical and Computer Engineering

## Group Members
-  Spencer Davis -- Software Engineering
-  Matt Dwyer -- Computer Engineering
-  Braedon Giblin -- Computer Engineering
-  Cody Tomkins -- Software Engineering
-  Prince Tshombe -- Electrical Engineering

## Project
This project features an exploration and expansion of a Xilinx QEMU and 
SystemC TLM cosimulation model. The model is designed to allow simulation
of software on QEMU, while memory mapped registers are updated via a SystemC
model.

## Repo
This repo is a meta repository of all required repositories executing a Linux
environment on the cosim model.  

## Installation and Usage

First clone the repo, recursing through submodules:

```
git clone --recurse-submodules <repo path>
```
Then, intall QEMU and SystemC Dependencies:
```
cd cosim-metarepo
sudo make install
```
Next, build the demo SystemC program and Buildroot
```
make
```
Finally, the demo can be run with:
```
./start_cosim
```

This runs each socket in a TMUX window. You may exit by selecting the right most window (CTRL+B, RIGHT_ARROW) and then pressing (CTRL + C)

### Oscilliscope

You may optionally install the [IIO oscilliscope](https://wiki.analog.com/resources/eval/user-guides/ad-fmcomms1-ebz/software/linux/applications/iio_scope) tool onto your local computer. After your model boots up, open a SSH port tunnel from QEMU back to your host:

```
ssh -R 30431:localhost:30431 -N username@hostpc
```

You should now be able to connect to the URI `ip:localhost` using IIO tools or the IIO oscilliscope to view data.
NOTE: Keep samples on oscilloscope to below 20
