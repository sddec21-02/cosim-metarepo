#!/bin/bash
root=$(git rev-parse --show-toplevel)
cd systemctlm-cosim-demo/
LD_LIBRARY_PATH=${root}/systemc-2.3.2/src/.libs/ ./zynq_demo unix:${root}/buildroot/handles/qemu-rport-_cosim@0 1000000
