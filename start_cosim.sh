#!/bin/bash

tmux new-session -d -s SystemC-CoSim "source exQemu.sh"
tmux split-window -d -t SystemC-CoSim:0 -p20 -v "source exSysc.sh" 


tmux select-layout -t SystemC-CoSim:0 even-horizontal
tmux attach-session -t SystemC-CoSim
